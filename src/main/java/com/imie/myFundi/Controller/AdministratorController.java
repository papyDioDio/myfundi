package com.imie.myFundi.Controller;

import com.imie.myFundi.Model.Administrator;
import com.imie.myFundi.Model.Customer;
import com.imie.myFundi.Model.LoginRequest;
import com.imie.myFundi.Service.AdministratorService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/administrator")
@AllArgsConstructor
public class AdministratorController {

    private final AdministratorService administratorService;

    @PostMapping("/create")
    public Administrator createAdministrator(@RequestBody Administrator administrator) {
        return administratorService.createAdministrator(administrator);
    }

    @GetMapping("/listAdministrators")
    public List<Administrator> getAllAdministrators() {
        return administratorService.getAllAdministrators();
    }

    @GetMapping("/get/{id}")
    public Administrator getAdministrator(@PathVariable Long id) {
        return administratorService.getAdministrator(id);
    }

    @PutMapping("/update/{id}")
    public Administrator update(@PathVariable Long id, @RequestBody Administrator administrator) {
        return administratorService.updateAdministratorInformation(id, administrator);
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        return administratorService.deleteAdministrator(id);
    }

    @PostMapping("login")
    public Administrator login(@RequestBody LoginRequest request){
        return administratorService.getAdministratorByCredentials(request.getEmail(), request.getPassword());
    }
}
