package com.imie.myFundi.Controller;

import com.imie.myFundi.Model.Appointment;
import com.imie.myFundi.Service.AppointmentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("appointment")
@AllArgsConstructor
public class AppointmentController {

    private final AppointmentService availabilityService;

    @PostMapping("/add")
    public Appointment addAppointment(@RequestBody Appointment appointment){
        return availabilityService.addAppointment(appointment);
    }

    @PostMapping("appointmentsByMechanic/{idMechanic}")
    public List<Appointment> getAppointmentsByMechanic(@PathVariable Long idMechanic){
        return availabilityService.getAppointmentsByMechanic(idMechanic);
    }

    @PostMapping("appointmentsByCustomer/{idCustomer}")
    public List<Appointment> getAppointmentsByCustomer(@PathVariable Long idCustomer){
        return availabilityService.getAppointmentsByCustomer(idCustomer);
    }

    @GetMapping("get/{id}")
    public Appointment getAppointment(@PathVariable Long id){
        return availabilityService.getAppointment(id);
    }

    @PutMapping("/update/{id}")
    public Appointment updateAppointment(@PathVariable Long id, @RequestBody Appointment availability){
        return availabilityService.updateAppointmentInformation(id, availability);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteAppointment(@PathVariable Long id){
        return availabilityService.deleteAppointment(id);
    }

    @DeleteMapping("/cancel/{id}")
    public String cancelAppointment(@PathVariable Long id){
        return availabilityService.cancelAppointment(id);
    }
}
