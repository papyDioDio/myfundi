package com.imie.myFundi.Controller;

import com.imie.myFundi.Model.Avaibality;
import com.imie.myFundi.Service.AvaibalityService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("availability")
@AllArgsConstructor
public class AvaibalityController {

    private final AvaibalityService availabilityService;

    @PostMapping("/add")
    public Avaibality addAvaibality(@RequestBody Avaibality availability){
        return availabilityService.addAvaibality(availability);
    }

    @PostMapping("availabilitiesByMechanic/{idMechanic}")
    public List<Avaibality> getAvaibalityByMechanic(@PathVariable Long idMechanic){
        return availabilityService.getAvaibalitiesByMechanic(idMechanic);
    }

    @GetMapping("get/{id}")
    public Avaibality getAvaibality(@PathVariable Long id){
        return availabilityService.getAvaibality(id);
    }

    @PutMapping("/update/{id}")
    public Avaibality updateAvaibality(@PathVariable Long id, @RequestBody Avaibality availability){
        return availabilityService.updateAvaibalityInformation(id, availability);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteAvaibality(@PathVariable Long id){
        return availabilityService.deleteAvaibality(id);
    }
}
