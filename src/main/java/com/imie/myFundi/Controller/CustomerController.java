package com.imie.myFundi.Controller;

import com.imie.myFundi.Model.Customer;
import com.imie.myFundi.Model.LoginRequest;
import com.imie.myFundi.Model.User;
import com.imie.myFundi.Service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
@AllArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping("/create")
    public Customer createCustomer(@RequestBody Customer customer) {
        return customerService.createCustomer(customer);
    }

    @GetMapping("/listCustomers")
    public List<Customer> getAllCustomers() {
        return customerService.getAllCustomers();
    }

    @GetMapping("/get/{id}")
    public Customer getCustomer(@PathVariable Long id) {
        return customerService.getCustomer(id);
    }

    @PutMapping("/update/{id}")
    public Customer update(@PathVariable Long id, @RequestBody Customer customer) {
        return customerService.updateCustomerInformation(id, customer);
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        return customerService.deleteCustomer(id);
    }

    @PostMapping("login")
    public Customer login(@RequestBody LoginRequest request){
        return customerService.getCustomerByCredentials(request.getEmail(), request.getPassword());
    }
}
