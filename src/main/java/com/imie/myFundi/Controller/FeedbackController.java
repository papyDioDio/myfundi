package com.imie.myFundi.Controller;

import com.imie.myFundi.Model.Feedback;
import com.imie.myFundi.Service.FeedbackService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/feedback")
@AllArgsConstructor
public class FeedbackController {

    private final FeedbackService feedbackService;

    @PostMapping("/add")
    public Feedback addFeedback(@RequestBody Feedback feedback){
        return feedbackService.addFeedback(feedback);
    }

    @PostMapping("feedbackByMechanic/{idMechanic}")
    public Float getFeedbackByMechanic(@PathVariable Long idMechanic){
        return feedbackService.getFeedbackByMechanic(idMechanic);
    }

    @GetMapping("get/{id}")
    public Feedback getFeedback(@PathVariable Long id){
        return feedbackService.getFeedback(id);
    }

    @PutMapping("/update/{id}")
    public Feedback updateFeedback(@PathVariable Long id, @RequestBody Feedback feedback){
        return feedbackService.updateFeedbackInformation(id, feedback);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteFeedback(@PathVariable Long id){
        return feedbackService.deleteFeedback(id);
    }
}
