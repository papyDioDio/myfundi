package com.imie.myFundi.Controller;

import com.imie.myFundi.Model.Customer;
import com.imie.myFundi.Model.LoginRequest;
import com.imie.myFundi.Model.Mechanic;
import com.imie.myFundi.Service.MechanicService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * @author papyDany
 */

@RestController
@RequestMapping("/mechanic")
@AllArgsConstructor
public class MechanicController {

    private final MechanicService mechanicService;
    @PostMapping("/create")
    public Mechanic createMechanic(@RequestBody Mechanic mechanic){
        return mechanicService.create(mechanic);
    }

    @GetMapping("listMechanics")
    public List<Mechanic> getListMechanics(){
        return mechanicService.getAllMechanics();
    }

    @GetMapping("get/{id}")
    public Mechanic getMechanic(@PathVariable Long id){
        return mechanicService.getMechanic(id);
    }

    @PutMapping("/update/{id}")
    public Mechanic update(@PathVariable Long id, @RequestBody Mechanic mechanic){
        return mechanicService.updateMechanicInformation(id,mechanic);
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        return mechanicService.deleteMechanic(id);
    }

    @PostMapping("login")
    public Mechanic login(@RequestBody LoginRequest request){
        return mechanicService.getMechanicByCredentials(request.getEmail(), request.getPassword());
    }
}
