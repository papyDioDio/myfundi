package com.imie.myFundi.Controller;

import com.imie.myFundi.Model.Role;
import com.imie.myFundi.Service.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * @author papyDany
 */

@RestController
@RequestMapping("role")
@AllArgsConstructor
public class RoleController {

    private final RoleService roleService;
    @PostMapping("/create")
    public Role createRole(@RequestBody Role role){
        return roleService.create(role);
    }

    @GetMapping("listRoles")
    public List<Role> getListRoles(){
        return roleService.getAllRoles();
    }

    @GetMapping("get/{id}")
    public Role getRole(@PathVariable Long id){
        return roleService.getRole(id);
    }

    @PutMapping("/update/{id}")
    public Role update(@PathVariable Long id, @RequestBody Role role){
        return roleService.updateRoleInformation(id,role);
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        return roleService.deleteRole(id);
    }
}
