package com.imie.myFundi.Controller;

import com.imie.myFundi.Model.Service;
import com.imie.myFundi.Service.ServiceService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * @author papyDany
 */

@RestController
@RequestMapping("/service")
@AllArgsConstructor
public class ServiceController {

    private final ServiceService serviceService;
    @PostMapping("/create")
    public Service createService(@RequestBody Service service){
        return serviceService.createService(service);
    }

    @GetMapping("listServices")
    public List<Service> getListServices(){
        return serviceService.getAllServices();
    }

    @GetMapping("get/{id}")
    public Service getService(@PathVariable Long id){
        return serviceService.getService(id);
    }

    @PutMapping("/update/{id}")
    public Service update(@PathVariable Long id, @RequestBody Service service){
        return serviceService.updateServiceInformation(id,service);
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        return serviceService.deleteService(id);
    }

    @PostMapping("listServices/{idMechanic}")
    public List<Service> getServicesByMechanic(@PathVariable Long idMechanic){
        return serviceService.getAllServicesByMechanic(idMechanic);
    }

}
