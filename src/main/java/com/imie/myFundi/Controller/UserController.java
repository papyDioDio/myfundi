package com.imie.myFundi.Controller;

import com.imie.myFundi.Model.LoginRequest;
import com.imie.myFundi.Model.User;
import com.imie.myFundi.Service.UserService;
import com.imie.myFundi.Exceptions.UserNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * @author papyDany
 */

@RestController
@RequestMapping("user")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/create")
    public Long createUser(@RequestBody User user){
        return userService.createUser(user);
    }

    @GetMapping("listUsers")
    public List<User> getListUsers(){
        return userService.getAllUsers();
    }

    @GetMapping("get/{id}")
    public User getUser(@PathVariable Long id){
        return userService.getUser(id);
    }

    @PutMapping("/update/{id}")
    public User update(@PathVariable Long id, @RequestBody User user){
        return userService.updateUserInformation(id, user);
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        return userService.deleteUser(id);
    }

    @PostMapping("login")
    public User login(@RequestBody LoginRequest request){
        return userService.getUserByCredentials(request.getEmail(), request.getPassword());
    }
}
