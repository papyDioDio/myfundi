package com.imie.myFundi.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 *
 * @author papyDany
 */
@Entity
@Table(name = "administrator")
@Getter
@Setter
@NoArgsConstructor
public class Administrator implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAdministrator;

    @Column(length = 50)
    private String prenomAdmin;

    @Column(length = 50)
    private String nomAdmin;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idUser")
    private User user;

}

