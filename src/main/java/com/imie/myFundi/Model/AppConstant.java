package com.imie.myFundi.Model;

public final class AppConstant {

    public static final int Mechanic = 1;
    public static final int Customer = 2;
    public static final int Administrator = 3;
    public static final int Available = 0;
    public static final int Unavailable = 1;
    public static final int Expire = 3;
    private AppConstant(){

    }
}
