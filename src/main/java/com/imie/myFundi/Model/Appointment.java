package com.imie.myFundi.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
@Entity
@Table(name = "appointment")
@Getter
@Setter
@NoArgsConstructor
public class Appointment implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAppointment;

    @ManyToOne
    @JoinColumn(name = "idService")
    private Service service;

    @ManyToOne
    @JoinColumn(name = "idCustomer")
    @JsonIgnoreProperties({"user"})
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "idTime")
    @JsonIgnoreProperties({"mechanic"})
    private Avaibality avaibality;
}
