package com.imie.myFundi.Model;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "avaibality")
@Getter
@Setter
@NoArgsConstructor
public class Avaibality implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idTime;

    @ManyToOne
    @JoinColumn(name = "idMechanic")
    private Mechanic mechanic;

    @JoinColumn(name = "date")
    private Date date;

    @JoinColumn(name = "creneau")
    private Long creneau;

    @JoinColumn(name = "state")
    private Long state;
}
