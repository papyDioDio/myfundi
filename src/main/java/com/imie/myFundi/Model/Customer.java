package com.imie.myFundi.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 *
 * @author papyDany
 */

@Entity
@Table(name = "customer")
@Getter
@Setter
@NoArgsConstructor
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCustomer;

    @Column(length = 50)
    private String prenomCustomer;

    @Column(length = 50)
    private String nomCustomer;

    @Column(length = 250)
    private String addresse;

    @Column
    private int numeroTelephone;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idUser")
    private User user;
}

