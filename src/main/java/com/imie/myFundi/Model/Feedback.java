package com.imie.myFundi.Model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 *
 * @author papyDany
 */
@Entity
@Table(name="Feedback")
@Getter
@Setter
@NoArgsConstructor
public class Feedback implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idFeedback;

    @Column(nullable = false)
    private int note;

    @ManyToOne
    @JoinColumn(name = "idMechanic")
    private Mechanic mechanic;

}