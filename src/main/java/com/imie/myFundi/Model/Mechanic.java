package com.imie.myFundi.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 *
 * @author papyDany
 */
@Entity
@Table(name="Mechanic")
@Getter
@Setter
@NoArgsConstructor
public class Mechanic implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idMechanic;

    @Column(length = 50)
    private String prenomMechanic;

    @Column(length = 50)
    private String nomMechanic;

    @Column(length = 250)
    private String addresse;

    @Column
    private int numeroTelephone;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idUser")
    private User user;

    //@OneToMany(mappedBy = "mechanic", cascade = CascadeType.ALL)
    //private List<Service> services;

    //@OneToMany(mappedBy = "mechanic", cascade = CascadeType.ALL)
    //private List<Feedback> feedbacks;

    //@OneToMany(mappedBy = "mechanic", cascade = CascadeType.ALL)
    //private List<AvaibalityRepository> avaibalities;

}

