package com.imie.myFundi.Model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Entity
@Table(name = "Service")
@Getter
@Setter
@NoArgsConstructor
public class Service implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idService;

    @Column(length = 50)
    private String nomService;

    private int tarif;

    @ManyToOne
    @JoinColumn(name = "idMechanic")
    @JsonIgnoreProperties({"user"})
    private Mechanic mechanic;
}
