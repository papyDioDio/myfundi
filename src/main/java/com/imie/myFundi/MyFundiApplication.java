package com.imie.myFundi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyFundiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyFundiApplication.class, args);
	}

}
