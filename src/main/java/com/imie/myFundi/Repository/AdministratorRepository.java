package com.imie.myFundi.Repository;

import com.imie.myFundi.Model.Administrator;
import com.imie.myFundi.Model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AdministratorRepository extends JpaRepository<Administrator,Long> {
    @Query("SELECT a FROM Administrator a WHERE a.user.idUser = :idUser")
    public Administrator findAdministratorByUser(@Param("idUser") Long idUser);
}
