package com.imie.myFundi.Repository;

import com.imie.myFundi.Model.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AppointmentRepository extends JpaRepository<Appointment,Long> {
    @Query("SELECT a FROM Appointment a WHERE a.service.mechanic.idMechanic = :idMechanic")
    public List<Appointment> getAppointmentsByMechanic(@Param("idMechanic") Long idMechanic);

    @Query("SELECT a FROM Appointment a WHERE a.customer.idCustomer = :idCustomer")
    public List<Appointment> getAppointmentsByCustomer(@Param("idCustomer") Long idCustomer);
}
