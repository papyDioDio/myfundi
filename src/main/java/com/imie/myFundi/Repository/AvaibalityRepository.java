package com.imie.myFundi.Repository;

import com.imie.myFundi.Model.Avaibality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AvaibalityRepository extends JpaRepository<Avaibality,Long> {
    @Query("SELECT a FROM Avaibality a WHERE a.mechanic.idMechanic = :idMechanic")
    public List<Avaibality> getAvaibalitiesByMechanic(@Param("idMechanic") Long idMechanic);

}
