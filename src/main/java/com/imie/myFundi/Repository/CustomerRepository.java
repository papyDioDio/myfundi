package com.imie.myFundi.Repository;

import com.imie.myFundi.Model.Customer;
import com.imie.myFundi.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CustomerRepository extends JpaRepository<Customer,Long> {
    @Query("SELECT c FROM Customer c WHERE c.user.idUser = :idUser")
    public Customer findCustomerByUser(@Param("idUser") Long idUser);

}
