package com.imie.myFundi.Repository;

import com.imie.myFundi.Model.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FeedbackRepository extends JpaRepository<Feedback,Long>{
    @Query("SELECT ROUND(CAST(sum(note) AS float)/CAST(count(idFeedback) AS float), 2) FROM Feedback f WHERE f.mechanic.idMechanic = :idMechanic")
    public Float getAverageFeedbackByMechanic(@Param("idMechanic") Long idMechanic);
}
