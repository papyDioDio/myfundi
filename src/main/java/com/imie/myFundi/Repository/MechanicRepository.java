package com.imie.myFundi.Repository;

import com.imie.myFundi.Model.Customer;
import com.imie.myFundi.Model.Mechanic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MechanicRepository extends JpaRepository<Mechanic,Long> {
    @Query("SELECT m FROM Mechanic m WHERE m.user.idUser = :idUser")
    public Mechanic findMechanicByUser(@Param("idUser") Long idUser);
}
