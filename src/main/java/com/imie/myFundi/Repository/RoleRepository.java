package com.imie.myFundi.Repository;
import com.imie.myFundi.Model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Long> {
}

