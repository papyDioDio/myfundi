package com.imie.myFundi.Repository;

import com.imie.myFundi.Model.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ServiceRepository extends JpaRepository<Service,Long> {
    @Query("SELECT s FROM Service s WHERE s.mechanic.idMechanic = :idMechanic")
    public List<Service> getAllServicesByMechanic(@Param("idMechanic") Long idMechanic);
}
