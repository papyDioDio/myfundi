package com.imie.myFundi.Service;

import com.imie.myFundi.Model.Administrator;
import com.imie.myFundi.Model.Customer;

import java.util.List;


public interface AdministratorService {
    public Administrator createAdministrator(Administrator administrator);
    public List<Administrator> getAllAdministrators();
    public Administrator getAdministrator(Long id);
    public Administrator getAdministratorByCredentials(String email, String password);
    public Administrator updateAdministratorInformation(Long id, Administrator administrator);
    public String deleteAdministrator(Long id);
}
