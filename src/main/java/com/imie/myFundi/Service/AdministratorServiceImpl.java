package com.imie.myFundi.Service;

import com.imie.myFundi.Model.Administrator;
import com.imie.myFundi.Model.AppConstant;
import com.imie.myFundi.Model.Customer;
import com.imie.myFundi.Model.User;
import com.imie.myFundi.Repository.AdministratorRepository;
import com.imie.myFundi.Repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AdministratorServiceImpl implements AdministratorService {

    private final AdministratorRepository administratorRepository;
    private final UserService userService;

    @Override
    public Administrator createAdministrator(Administrator administrator) {
        return administratorRepository.save(administrator);
    }

    @Override
    public List<Administrator> getAllAdministrators() {
        return administratorRepository.findAll();
    }

    @Override
    public Administrator getAdministrator(Long id) {
        return administratorRepository.findById(id).orElseThrow(() -> new RuntimeException("Administrateur non trouvé !"));
    }

    @Override
    public String deleteAdministrator(Long id) {
        try {
            administratorRepository.deleteById(id);
        } catch (Exception err) {
            return "Administrateur non reconnu";
        }

        return "Administrateur supprimé";
    }

    @Override
    public Administrator updateAdministratorInformation(Long id, Administrator administrator) {
        return administratorRepository.findById(id)
                .map(a -> {
                    a.setNomAdmin(administrator.getNomAdmin());
                    a.setPrenomAdmin(administrator.getPrenomAdmin());
                    return administratorRepository.save(a);
                }).orElseThrow(() -> new RuntimeException("Administrateur non trouvé !"));
    }

    @Override
    public Administrator getAdministratorByCredentials(String email, String password){
        User user = userService.getUserByCredentials(email,password);
        if((user != null) && (user.getRole().getIdRole() == AppConstant.Administrator)){
            return administratorRepository.findAdministratorByUser(user.getIdUser());
        }
        else {
            return null;
        }
    }
}
