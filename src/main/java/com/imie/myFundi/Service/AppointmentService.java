package com.imie.myFundi.Service;

import com.imie.myFundi.Model.Appointment;

import java.util.List;

public interface AppointmentService {
    public Appointment getAppointment(Long id);
    public Appointment updateAppointmentInformation(Long id, Appointment appointment);
    public String deleteAppointment(Long id);
    public String cancelAppointment(Long id);
    public Appointment addAppointment(Appointment appointment);
    public List<Appointment> getAppointmentsByMechanic(Long idMechanic);
    public List<Appointment> getAppointmentsByCustomer(Long idCustomer);
}