package com.imie.myFundi.Service;

import com.imie.myFundi.Model.AppConstant;
import com.imie.myFundi.Model.Appointment;
import com.imie.myFundi.Model.Avaibality;
import com.imie.myFundi.Repository.AppointmentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AppointmentServiceImpl implements AppointmentService{
    private final AppointmentRepository appointmentRepository;
    private final AvaibalityService avaibalityService;
    @Override
    public Appointment getAppointment(Long id){
        return appointmentRepository.findById(id).orElseThrow(() -> new RuntimeException("rendez-vous non trouvé !"));
    }
    @Override
    public Appointment updateAppointmentInformation(Long id, Appointment appointment){
        return appointmentRepository.findById(id)
                .map(a -> {
                    a.setCustomer(appointment.getCustomer());
                    a.setService(appointment.getService());
                    a.setAvaibality(appointment.getAvaibality());
                    return appointmentRepository.save(a);
                }).orElseThrow(() -> new RuntimeException("rendez-vous non trouvé !"));
    }
    @Override
    public String deleteAppointment(Long id){
        try {
            Appointment appointment = getAppointment(id);
            Long idAvaibality = appointment.getAvaibality().getIdTime();
            Avaibality avaibality = avaibalityService.getAvaibality(idAvaibality);
            avaibality.setState((long) AppConstant.Expire);
            avaibalityService.updateAvaibalityInformation(idAvaibality,avaibality);
            appointmentRepository.deleteById(id);
        } catch (Exception err) {
            return "rendez-vous non reconnu";
        }
        return "rendez-vous supprimé";
    }
    @Override
    public String cancelAppointment(Long id){
        try {
            Appointment appointment = getAppointment(id);
            Long idAvaibality = appointment.getAvaibality().getIdTime();
            Avaibality avaibality = avaibalityService.getAvaibality(idAvaibality);
            avaibality.setState((long) AppConstant.Available);
            avaibalityService.updateAvaibalityInformation(idAvaibality,avaibality);
            appointmentRepository.deleteById(id);
        } catch (Exception err) {
            return "rendez-vous non reconnu";
        }
        return "rendez-vous annulé";
    }
    @Override
    public Appointment addAppointment(Appointment appointment){
        Appointment result = appointmentRepository.save(appointment);
        if(result != null){
            Long idAvaibality = appointment.getAvaibality().getIdTime();
            Avaibality avaibality = avaibalityService.getAvaibality(idAvaibality);
            avaibality.setState((long) AppConstant.Unavailable);
            avaibalityService.updateAvaibalityInformation(idAvaibality,avaibality);
            return result;
        }
        return null;
    }
    @Override
    public List<Appointment> getAppointmentsByMechanic(Long idMechanic){
        return appointmentRepository.getAppointmentsByMechanic(idMechanic);
    }
    @Override
    public List<Appointment> getAppointmentsByCustomer(Long idCustomer){
        return appointmentRepository.getAppointmentsByCustomer(idCustomer);
    }
}
