package com.imie.myFundi.Service;

import com.imie.myFundi.Model.Avaibality;

import java.util.List;

public interface AvaibalityService {
    public Avaibality getAvaibality(Long id);
    public Avaibality updateAvaibalityInformation(Long id, Avaibality avaibality);
    public String deleteAvaibality(Long id);
    public Avaibality addAvaibality(Avaibality avaibality);
    public List<Avaibality> getAvaibalitiesByMechanic(Long idMechanic);
}
