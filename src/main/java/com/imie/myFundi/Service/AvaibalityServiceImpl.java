package com.imie.myFundi.Service;

import com.imie.myFundi.Model.AppConstant;
import com.imie.myFundi.Model.Avaibality;
import com.imie.myFundi.Repository.AvaibalityRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AvaibalityServiceImpl implements AvaibalityService{
    private final AvaibalityRepository avaibalityRepository;
    public Avaibality getAvaibality(Long id){
        return avaibalityRepository.findById(id).orElseThrow(() -> new RuntimeException("disponibilité non trouvé !"));
    }
    public Avaibality updateAvaibalityInformation(Long id, Avaibality avaibality){
        return avaibalityRepository.findById(id)
                .map(a -> {
                    a.setDate(avaibality.getDate());
                    a.setMechanic(avaibality.getMechanic());
                    a.setCreneau(avaibality.getCreneau());
                    return avaibalityRepository.save(a);
                }).orElseThrow(() -> new RuntimeException("disponibilité non trouvée !"));
    }
    @Override
    public String deleteAvaibality(Long id){
        try {
            avaibalityRepository.deleteById(id);
        } catch (Exception err) {
            return "disponibilité non reconnue";
        }
        return "disponibilité supprimée";
    }
    public Avaibality addAvaibality(Avaibality avaibality){
        avaibality.setState((long) AppConstant.Available);
        return avaibalityRepository.save(avaibality);
    }
    public List<Avaibality> getAvaibalitiesByMechanic(Long idMechanic){
        return avaibalityRepository.getAvaibalitiesByMechanic(idMechanic);
    }
}
