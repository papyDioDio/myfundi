package com.imie.myFundi.Service;

import com.imie.myFundi.Model.Customer;
import com.imie.myFundi.Model.User;

import java.util.List;


public interface CustomerService {
    public Customer createCustomer(Customer mechanic);
    public List<Customer> getAllCustomers();
    public Customer getCustomer(Long id);
    public Customer getCustomerByCredentials(String email, String password);
    public Customer updateCustomerInformation(Long id, Customer mechanic);
    public String deleteCustomer(Long id);

}
