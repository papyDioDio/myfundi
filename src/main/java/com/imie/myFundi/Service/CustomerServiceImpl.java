package com.imie.myFundi.Service;

import com.imie.myFundi.Model.AppConstant;
import com.imie.myFundi.Model.Customer;
import com.imie.myFundi.Model.User;
import com.imie.myFundi.Repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final UserService userService;


    @Override
    public Customer createCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    public Customer getCustomer(Long id) {
        return customerRepository.findById(id).orElseThrow(() -> new RuntimeException("Client non trouvé !"));
    }

    @Override
    public String deleteCustomer(Long id) {
        try {
            customerRepository.deleteById(id);
        } catch (Exception err) {
            return "Client non reconnu";
        }

        return "Client supprimé";
    }

    @Override
    public Customer updateCustomerInformation(Long id, Customer customer) {
        return customerRepository.findById(id)
                .map(c -> {
                    c.setNomCustomer(customer.getNomCustomer());
                    c.setPrenomCustomer(customer.getPrenomCustomer());
                    c.setAddresse(customer.getAddresse());
                    c.setNumeroTelephone(customer.getNumeroTelephone());
                    return customerRepository.save(c);
                }).orElseThrow(() -> new RuntimeException("Client non trouvé !"));
    }

    public String getCustomerEmail(Long customerId) {
        Customer customer = customerRepository.findById(customerId).orElseThrow();

        User user = customer.getUser();
        return user.getEmail();
    }

    @Override
    public Customer getCustomerByCredentials(String email,String password){
        User user = userService.getUserByCredentials(email,password);
        if((user != null) && (user.getRole().getIdRole() == AppConstant.Customer)){
            return customerRepository.findCustomerByUser(user.getIdUser());
        }
        else {
            return null;
        }
    }
}
