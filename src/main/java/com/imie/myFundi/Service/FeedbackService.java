package com.imie.myFundi.Service;

import com.imie.myFundi.Model.Feedback;

import java.util.List;

public interface FeedbackService {
    public Feedback create(Feedback feedback);
    public Feedback getFeedback(Long id);
    public Feedback updateFeedbackInformation(Long id, Feedback feedback);
    public String deleteFeedback(Long id);
    public Feedback addFeedback(Feedback feedback);
    public Float getFeedbackByMechanic(Long idMechanic);
}
