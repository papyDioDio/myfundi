package com.imie.myFundi.Service;

import com.imie.myFundi.Model.Feedback;
import com.imie.myFundi.Repository.FeedbackRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository feedbackRepository;

    @Override
    public Feedback create(Feedback feedback) {
        return feedbackRepository.save(feedback);
    }


    @Override
    public Feedback getFeedback(Long id) {
        return feedbackRepository.findById(id).orElseThrow(() -> new RuntimeException("note non trouvé !"));
    }

    @Override
    public Feedback updateFeedbackInformation(Long id, Feedback feedback) {
        return feedbackRepository.findById(id)
                .map(f -> {
                    f.setNote(feedback.getNote());
                    f.setMechanic(feedback.getMechanic());
                    return feedbackRepository.save(f);
                }).orElseThrow(() -> new RuntimeException("note non trouvée !"));
    }

    @Override
    public String deleteFeedback(Long id) {
        try {
            feedbackRepository.deleteById(id);
        } catch (Exception err) {
            return "commentaire non reconnu";
        }
        return "commentaire supprimé";
    }

    @Override
    public Float getFeedbackByMechanic(Long idMechanic){
        Float note = feedbackRepository.getAverageFeedbackByMechanic(idMechanic);
        return (note == null ? null : note);
    }

    @Override
    public Feedback addFeedback(Feedback feedback){
        return feedbackRepository.save(feedback);
    }
}
