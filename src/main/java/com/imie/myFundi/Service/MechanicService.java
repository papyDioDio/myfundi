package com.imie.myFundi.Service;

import com.imie.myFundi.Model.Customer;
import com.imie.myFundi.Model.Mechanic;

import java.util.List;


public interface MechanicService {
    public Mechanic create(Mechanic mechanic);
    public List<Mechanic> getAllMechanics();
    public Mechanic getMechanic(Long id);
    public Mechanic getMechanicByCredentials(String email, String password);
    public Mechanic updateMechanicInformation(Long id, Mechanic mechanic);
    public String deleteMechanic(Long id);
}
