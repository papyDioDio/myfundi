package com.imie.myFundi.Service;

import com.imie.myFundi.Model.AppConstant;
import com.imie.myFundi.Model.Customer;
import com.imie.myFundi.Model.Mechanic;
import com.imie.myFundi.Model.User;
import com.imie.myFundi.Repository.MechanicRepository;
import com.imie.myFundi.Repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MechanicServiceImpl implements MechanicService {

     private final MechanicRepository mechanicRepository;
     private final UserService userService;

     @Override
     public Mechanic create(Mechanic mechanic){
         return mechanicRepository.save(mechanic);
     }

     @Override
    public List<Mechanic> getAllMechanics(){
         return mechanicRepository.findAll();
     }

     @Override
     public Mechanic getMechanic(Long id){
             return mechanicRepository.findById(id).orElseThrow(() -> new RuntimeException("mécanicien non trouvé !"));
     }

     @Override
    public String deleteMechanic(Long id){
         try{
             mechanicRepository.deleteById(id);
         } catch (Exception err) {
             return "mécanicien non reconnu";
         }

         return "mécanicien supprimé";
     }

     @Override
    public Mechanic updateMechanicInformation(Long id, Mechanic classe){
         return mechanicRepository.findById(id)
                 .map(c->{
                     c.setNomMechanic(classe.getNomMechanic());
                     c.setPrenomMechanic(classe.getPrenomMechanic());
                     c.setAddresse(classe.getAddresse());
                     c.setNumeroTelephone(classe.getNumeroTelephone());
                     return mechanicRepository.save(c);
                 }).orElseThrow(() -> new RuntimeException("mécanicien non trouvé !"));
     }

    @Override
    public Mechanic getMechanicByCredentials(String email, String password){
        User user = userService.getUserByCredentials(email,password);
        if((user != null) && (user.getRole().getIdRole() == AppConstant.Mechanic)){
            return mechanicRepository.findMechanicByUser(user.getIdUser());
        }
        else {
            return null;
        }
    }
}
