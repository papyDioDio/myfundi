package com.imie.myFundi.Service;

import com.imie.myFundi.Model.Role;

import java.util.List;

public interface RoleService {
    public Role create(Role role);
    public List<Role> getAllRoles();
    public Role getRole(Long id);
    public Role updateRoleInformation(Long id, Role role);
    public String deleteRole(Long id);
}
