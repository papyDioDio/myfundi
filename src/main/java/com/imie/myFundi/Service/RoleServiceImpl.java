package com.imie.myFundi.Service;

import com.imie.myFundi.Model.Role;
import com.imie.myFundi.Repository.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;
    @Override
    public Role create(Role role){
         return roleRepository.save(role);
     }
    @Override
    public List<Role> getAllRoles(){
        return roleRepository.findAll();
    }
    @Override
    public Role getRole(Long id){
        return null;
     }

    @Override
    public Role updateRoleInformation(Long id, Role role){
        return null;
    }
    @Override
    public String deleteRole(Long id){
        return null;
    }
}
