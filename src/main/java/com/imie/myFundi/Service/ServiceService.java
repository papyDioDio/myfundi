package com.imie.myFundi.Service;

import com.imie.myFundi.Model.Mechanic;
import com.imie.myFundi.Model.Service;

import java.util.List;

public interface ServiceService {
    public Service createService(Service service);
    public List<Service> getAllServices();
    public Service getService(Long id);
    public Service updateServiceInformation(Long id, Service service);
    public String deleteService(Long id);
    public List<Service> getAllServicesByMechanic(Long idMechanic);
}