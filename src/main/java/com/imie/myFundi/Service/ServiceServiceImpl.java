package com.imie.myFundi.Service;

import com.imie.myFundi.Model.Service;
import com.imie.myFundi.Repository.ServiceRepository;
import lombok.AllArgsConstructor;

import java.util.List;
@org.springframework.stereotype.Service
@AllArgsConstructor
public class ServiceServiceImpl implements ServiceService{
    private final ServiceRepository serviceRepository;
    @Override
    public Service createService(Service service){
        return serviceRepository.save(service);
    }
    @Override
    public List<Service> getAllServices(){
        return serviceRepository.findAll();
    }
    @Override
    public Service getService(Long id){
        return serviceRepository.findById(id).orElseThrow(() -> new RuntimeException("service non trouvé !"));
    }
    @Override
    public Service updateServiceInformation(Long id, Service service){
        return serviceRepository.findById(id)
                .map(s -> {
                    s.setTarif(service.getTarif());
                    s.setNomService(service.getNomService());
                    s.setMechanic(service.getMechanic());
                    return serviceRepository.save(s);
                }).orElseThrow(() -> new RuntimeException("service non trouvé !"));
    }
    @Override
    public String deleteService(Long id){
        try {
            serviceRepository.deleteById(id);
        } catch (Exception err) {
            return "Service non reconnu";
        }

        return "Service supprimé";
    }

    @Override
    public List<Service> getAllServicesByMechanic(Long idMechanic){
        return serviceRepository.getAllServicesByMechanic(idMechanic);
    }
}
