package com.imie.myFundi.Service;

import com.imie.myFundi.Model.User;

import java.util.List;

public interface UserService {
    public Long createUser(User user);
    public List<User> getAllUsers();
    public User getUser(Long id);
    public User getUserByCredentials(String email,String password);
    public User updateUserInformation(Long id, User user);
    public String deleteUser(Long id);
}
