package com.imie.myFundi.Service;

import com.imie.myFundi.Model.User;
import com.imie.myFundi.Repository.UserRepository;
import com.imie.myFundi.Service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public Long createUser(User user) {
        User newUser = userRepository.save(user);
        return newUser.getIdUser();
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUser(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new RuntimeException("Utilisateur non trouvé !"));
    }

    @Override
    public String deleteUser(Long id) {
        try {
            userRepository.deleteById(id);
        } catch (Exception err) {
            return "Utilisateur non reconnu";
        }

        return "Utilisateur supprimé";
    }

    @Override
    public User updateUserInformation(Long id, User user) {
        return userRepository.findById(id)
                .map(u -> {
                    u.setPassword(user.getPassword());
                    u.setEmail(user.getEmail());
                    return userRepository.save(u);
                }).orElseThrow(() -> new RuntimeException("Utilisateur non trouvé !"));
    }

    @Override
    public User getUserByCredentials(String email,String password){
        User user = userRepository.findUserByEmail(email);
        if (user.getPassword().equals(password))
            return user;
        else
            return null;
    }
}
